<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\annotations\reader
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\format\driver\json\JSON;
	use \ReflectionClass;
	
	/**
	 * Annotation Reader plugin.
	 * 
	 * This class is used to read and parse annotations assosiated with a class using the ReflectionClass.
	 * 
	 * @package    nuclio\plugin\annotations\reader
	 */
	<<factory>>
	class Reader extends Plugin
	{
		const ANNOTATION_REGEX	='/@(\w+)(?:\s*(?:\(\s*)?(.*?)(?:\s*\))?)??\s*(?:\n|\*\/)/';
		const PARAMETER_REGEX	='/(\w+)\s*=\s*(\[[^\]]*\]|"[^"]*"|[^,)]*)\s*(?:,|$)/';
		/**
		 * @var Map<string,mixed> variable that contains all annotations generated by the ReflectionClass for the selected class.
		 * @access private
		 */
		private Map<string,mixed> $annotations;
		/**
		 * Get the instance.
		 *
		 * @param      ...     $args   argument to be sent to the constructor (mixed $class) {@link __construct()}
		 * @access public
		 * @static
		 * @return     Router  Instance.
		 */
		public static function getInstance(/* HH_FIXME[4033] */...$args):Reader
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}
		
		/**
		 * Parses all annotations found in a given class using the ReflectionClass.
		 * 
		 * @param mixed $class A reference to the class.
		 * @access public
		 */
		public function __construct(mixed $class):void
		{
			parent::__construct();
			$reflection		=new ReflectionClass($class);
			$classBlock		=$reflection->getDocComment();
			$propertyBlocks	=[];
			
			foreach ($reflection->getProperties() as $property)
			{
				$propertyBlocks[$property->getName()]=$property->getDocComment();
			}
			
			$this->annotations=Map
			{
				'class'=>$this->parseDocComment((string)$classBlock)
			};
			foreach ($propertyBlocks as $name=>$property)
			{
				if (is_string($property))
				{
					$this->annotations->set($name,$this->parseDocComment($property));
				}
			}
		}
		/**
		 * Return the parsed annotations list.
		 * 
		 * The returned map object represent a key/value pairs for each annotation exists on the class definition.
		 * 
		 * @return Map<string,mixed> The annotations which were parsed from the class definition.
		 * @access public
		 */
		public function getAnnotations():Map<string,mixed>
		{
			return $this->annotations;
		}
		
		/**
		 * Parse annotation in a given doc comment.
		 * 
		 * Parse any annotations from the given doc comment and return them in a
		 * Map structure.  The annotations in the returned Map are indexed by
		 * their lower cased name.  Parameters with a value defined as a comma
		 * separated list contained in braces will be return as Vector.  Parameter
		 * values defined in quotes will have the quotes stripped and the inner value
		 * parsed for boolean and numeric values.  If not a boolean or numeric value,
		 * will be return as a string.	All other parameter values will be returned as
		 * either a boolean, number or string as appropriate.
		 *
		 * @param string $docComment The comment to parse.
		 * @return Map<string,mixed> Array containing the defined annotations.
		 * @internal the class uses the private methods {@link parseValue()}.
		 * @access private
		 */
		private function parseDocComment(string $docComment):Map<string,mixed>
		{
			$matches=[];
			$hasAnnotations=preg_match_all
			(
				self::ANNOTATION_REGEX,
				$docComment,
				&$matches,
				PREG_SET_ORDER
			);
			if (!$hasAnnotations)
			{
				return new Map(null);
			}
			$annotations=new Map(null);
			foreach ($matches as $anno)
			{
				$anno		=new Vector($anno);
				$annoName	=strtolower($anno[1]);
				$val		=true;
				if ($anno->containsKey(2))
				{
					$params=[];
					$hasParams=preg_match_all(self::PARAMETER_REGEX,$anno[2],&$params,PREG_SET_ORDER);
					if ($hasParams)
					{
						$val=new Map(null);
						foreach ($params as $param)
						{
							$val->set($param[1],$this->parseValue($param[2]));
						}
					}
					else
					{
						$val=trim($anno[2]);
						if ($val=='')
						{
							$val=true;
						}
						else
						{
							$val=$this->parseValue($val);
						}
					}
				}
				if ($annotations->containsKey($annoName))
				{
					if (!$annotations->get($annoName) instanceof Vector)
					{
						$annotations->set($annoName,Vector{$annotations[$annoName]});
					}
					$vector=$annotations->get($annoName);
					if ($vector instanceof Vector && !is_null($vector))
					{
						$annotations->set($annoName,$vector);
					}
				}
				else
				{
					$annotations[$annoName]=$val;
				}
			}
			return $annotations;
		}
		
		/**
		 * Parse the value of the annotation.
		 * 
		 * This function try to check the type of the value either it is Array,Json object,string,boolean,numeric.
		 * Note : this function will be called recursivly in case the raw value sent to it was starting with `"` and ending with `"`.
		 * 
		 * @param string $value The raw value of the annotation to be parsed.
		 * @return mixed An object that can be :
		 * 		Vector : in case the raw value was a string starting with `[` and ending with `]` and consisted of comma seperated values.
		 *		Json decoded object (mixed) : in case the raw value was string starting with `{` and ending with `}`.
		 *		Boolean : in case the raw value was `true` or `false`.
		 *		Number : in case the raw value was a number.
		 *		String : in all other cases.
		 * @access private
		 */
		private function parseValue(string $value):mixed
		{
			$val=trim($value);
			if (substr($val,0,1)=='[' && substr($val,-1)==']')
			{
				// Array values
				$vals	=explode(',',substr($val,1,-1));
				$val	=new Vector(null);
				foreach ($vals as $v)
				{
					$val->add($this->parseValue($v));
				}
				return $val;
			}
			else if (substr($val,0,1)=='{' && substr($val,-1)=='}')
			{
				// If is json object that start with { } decode them
				// FormatReader::getInstance();
				// $JSONReader=ProviderManager::request('format::JSON');
				$JSONReader=JSON::getInstance();
				return $JSONReader->decode($val);
				// return json_decode($val);
			}
			else if (substr($val,0,1)=='"' && substr($val,-1)=='"')
			{
				// Quoted value,remove the quotes then recursively parse and return
				$val=substr($val,1,-1);
				return $this->parseValue($val);
			}
			else if (strtolower($val)=='true')
			{
				// Boolean value=true
				return true;
			}
			else if (strtolower($val)=='false')
			{
				// Boolean value=false
				return false;
			}
			else if (is_numeric($val))
			{
				// Numeric value,determine if int or float and then cast
				if ((float) $val==(int)$val)
				{
					return (int)$val;
				}
				else
				{
					return (float)$val;
				}
			}
			else
			{
				// Nothing special,just return as a string
				return $val;
			}
		}
	}
}
